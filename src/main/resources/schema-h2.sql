DROP TABLE IF EXISTS customers;
CREATE TABLE public.customers (
                               id INT AUTO_INCREMENT PRIMARY KEY,
                               name VARCHAR(250) NOT NULL,
                               email VARCHAR(250) NOT NULL,
                               age INT NOT NULL,
                               phone VARCHAR(20),
                               password VARCHAR(50) NOT NULL,
                               created_date TIMESTAMP,
                               last_modified_date TIMESTAMP
);

DROP TABLE IF EXISTS accounts;
CREATE TABLE public.accounts (
                       id INTEGER AUTO_INCREMENT PRIMARY KEY,
                       number VARCHAR(250) NOT NULL,
                       currency VARCHAR(3) NOT NULL,
                       balance NUMERIC (12,2) NOT NULL DEFAULT 0,
                       customer_id INTEGER REFERENCES customers (id),
                       created_date TIMESTAMP,
                       last_modified_date TIMESTAMP
);

DROP TABLE IF EXISTS employers;
CREATE TABLE public.employers (
                       id INTEGER AUTO_INCREMENT PRIMARY KEY,
                       name VARCHAR(250) NOT NULL,
                       address VARCHAR(250) NOT NULL,
                       created_date TIMESTAMP,
                       last_modified_date TIMESTAMP
);

DROP TABLE IF EXISTS customerEmployment;
CREATE TABLE public.customerEmployment (
                       id INTEGER AUTO_INCREMENT PRIMARY KEY,
                       customer_id INTEGER REFERENCES customers (id) NOT NULL,
                       employer_id INTEGER REFERENCES employers (id) NOT NULL,
                       created_date TIMESTAMP,
                       last_modified_date TIMESTAMP
);
