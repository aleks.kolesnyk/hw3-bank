package HW3.service;

import HW3.dao.EmployerJpaRepository;
import HW3.model.Employer;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class EmployerService {

    private final EmployerJpaRepository employerJpaRepository;

    public Employer getEmployerFullInfo(Long id){
        return employerJpaRepository.getOne(id);
    }

    public List<Employer> getAllEmployers(){
        return employerJpaRepository.findAll();
    }

    public Employer createEmployer(Employer newEmployer){
        return employerJpaRepository.save(newEmployer);
    }
    public Employer createEmployer(String name, String address){
        Employer newEmployer = new Employer(name, address);
        return employerJpaRepository.save(newEmployer);
    }

    public Employer updateEmployer(Employer employer){
        if(employer.getId() == null){
            return null;
        }
        return employerJpaRepository.save(employer);
    }

    public Employer updateEmployer(Long id, String name, String address){
        if(id == null){
            return null;
        }
        Employer employer = new Employer(id, name, address);
        return employerJpaRepository.save(employer);
    }

    public void deleteEmployer(Employer employer){
        employerJpaRepository.delete(employer);
    }
    public boolean deleteEmployerById(Long id){
        Employer currentEmployer = this.getEmployerFullInfo(id);
        this.deleteEmployer(currentEmployer);
        return true;
    }
}

