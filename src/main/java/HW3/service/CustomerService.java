package HW3.service;

import HW3.dao.AccountJpaRepository;
import HW3.dao.CustomerJpaRepository;
import HW3.model.Account;
import HW3.model.Currency;
import HW3.model.Customer;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class CustomerService {

    private final CustomerJpaRepository customerJpaRepository;
    private final AccountJpaRepository accountJpaRepository;


    public Customer getCustomerFullInfo(Long id){
        return customerJpaRepository.findById(id).get();
    }

    public List<Customer> getAllCustomers(Integer  page, Integer size){
        Sort sort = Sort.by(new Sort.Order(Sort.Direction.ASC, "id"));
        Pageable pageable = (Pageable) PageRequest.of(page, size, sort);
        Page<Customer> customerPage = customerJpaRepository.findAll(pageable);
        return customerPage.toList();
    }
    public List<Customer> getAllCustomers(){
        return getAllCustomers(0, 10);
    }
    public Customer createCustomer(Customer newCustomer){
        return customerJpaRepository.save(newCustomer);
    }
    public Customer createCustomer(String name, String email, Integer age){
        Customer newCustomer = new Customer(name, email, age);
        return customerJpaRepository.save(newCustomer);
    }

    public Customer updateCustomer(Customer customer){
        if(customer.getId() == null){
            return null;
        }
        return customerJpaRepository.save(customer);
    }

    public Customer updateCustomer(Long id, String name, String email, Integer age){
        if(id == null){
            return null;
        }
        Customer customer = new Customer(name, email, age);
        customer.setId(id);
        return customerJpaRepository.save(customer);
    }

    public void deleteCustomer(Customer customer){
        customerJpaRepository.delete(customer);
    }
    public boolean deleteCustomerById(Long id){
        Customer currentCustomer = this.getCustomerFullInfo(id);
        this.deleteCustomer(currentCustomer);
        return true;
    }
    public Customer openAccount(Customer customer, Currency currency){
        Account newAccount = new Account(customer, currency);
        accountJpaRepository.save(newAccount);
        return this.getCustomerFullInfo(customer.getId());
    }
    public Customer deleteAccount(Customer customer, String number){
        System.out.println(number);
        Account account = accountJpaRepository.findByNumber(number);
        System.out.println(account);
        if(account.getCustomer().equals(customer)){
            accountJpaRepository.deleteById(account.getId());
        }
        return this.getCustomerFullInfo(customer.getId());
    }
}

