package HW3.controller;

import HW3.model.DTO.EmployerDtoMapperRequest;
import HW3.model.DTO.EmployerDtoMapperResponse;
import HW3.model.DTO.EmployerDtoRequest;
import HW3.model.Employer;
import HW3.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/employers")
@RequiredArgsConstructor
public class EmployerController {

    private final EmployerService employerService;
    private final EmployerDtoMapperRequest employerDtoMapperRequest;
    private final EmployerDtoMapperResponse employerDtoMapperResponse;

    @GetMapping("/")
    public ResponseEntity<?> getAllEmployers() {

        return ResponseEntity.ok(employerService.getAllEmployers().stream()
                .map(employerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id) {

        Employer employer = employerService.getEmployerFullInfo(id);
        return employer != null
                ? ResponseEntity.ok().body(employerDtoMapperResponse.convertToDto(employer))
                : ResponseEntity.badRequest().body("Employer with id " + id + " not found");
    }

    @PostMapping("/")
    public ResponseEntity<?> postNewUser(@RequestBody EmployerDtoRequest employerDtoRequest) {

        Employer employer = employerDtoMapperRequest.convertToEntity(employerDtoRequest);
        Employer newEmployer = employerService.createEmployer(employer);
        return newEmployer != null
                ? ResponseEntity.ok().body(employerDtoMapperResponse.convertToDto(newEmployer))
                : ResponseEntity.badRequest().body("Saving of the new employer has fail");
    }

    @PutMapping("/")
    public ResponseEntity<?> updateUser(@RequestBody EmployerDtoRequest employerDtoRequest) {

        Employer employer = employerDtoMapperRequest.convertToEntity(employerDtoRequest);
        Employer newEmployer = employerService.updateEmployer(employer);
        return newEmployer != null
                ? ResponseEntity.ok().body(employerDtoMapperResponse.convertToDto(newEmployer))
                : ResponseEntity.badRequest().body("Updating of the employer has fail");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id) {

        return employerService.deleteEmployerById(id)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Employer not found");
    }
}


