package HW3.controller;

import HW3.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;


    @PostMapping("/deposit")
    public ResponseEntity<?> depositMoney(@RequestBody String accountNumber, Double amount) {

        return accountService.depositAccount(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Depositing of the account is failed");
    }

    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawalMoney(@RequestBody String accountNumber, Double amount) {

        return accountService.withdrawalMoney(accountNumber, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Withdrawing from the account is failed");
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendMoney(@RequestBody String accountNumberSender, String accountNumberReceiver, Double amount) {

        return accountService.sendMoney(accountNumberSender, accountNumberReceiver, amount)
                ? ResponseEntity.ok("Success")
                : ResponseEntity.badRequest().body("Sending money is failed");
    }
}

