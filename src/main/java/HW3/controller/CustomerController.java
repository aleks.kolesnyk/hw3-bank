package HW3.controller;

import HW3.model.Currency;
import HW3.model.Customer;
import HW3.model.CustomerDtoMapperResponse;
import HW3.model.DTO.CustomerDtoMapperRequest;
import HW3.model.DTO.CustomerDtoRequest;
import HW3.model.DTO.CustomerDtoResponse;
import HW3.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerDtoMapperResponse customerDtoMapperResponse;
    private final CustomerDtoMapperRequest customerDtoMapperRequest;

    @GetMapping("/")
    public ResponseEntity<?> getAllUsers() {
        return ResponseEntity.ok(customerService.getAllCustomers().stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{page}/{size}")
    public ResponseEntity<?> getAllUsers(@PathVariable Integer page, Integer size) {
        return ResponseEntity.ok(customerService.getAllCustomers(page, size).stream()
                .map(customerDtoMapperResponse::convertToDto)
                .collect(Collectors.toList()));
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserFullInfo(@PathVariable Long id){
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.getCustomerFullInfo(id)));
    }
    @PostMapping("/")
    public CustomerDtoResponse postNewUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        return customerDtoMapperResponse.convertToDto(customerService.createCustomer(customer));
    }
    @PutMapping("/")
    public CustomerDtoResponse updateUser(@RequestBody CustomerDtoRequest customerDtoRequest){
        Customer customer = customerDtoMapperRequest.convertToEntity(customerDtoRequest);
        return customerDtoMapperResponse.convertToDto(customerService.updateCustomer(customer));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteCustomer(@PathVariable Long id){
        return customerService.deleteCustomerById(id) ? ResponseEntity.ok("Success") : ResponseEntity.badRequest().body("Customer not found");
    }
    @PostMapping("/{id}/account")
    public ResponseEntity<?> openAccount(@PathVariable Long id, @RequestBody String currency) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode newAccountCurrency = objectMapper.readTree(currency);
        String curren = newAccountCurrency.get("currency").asText();
        Currency curr = Currency.forValue(curren);

        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.openAccount(customer, curr)));
    }
    @DeleteMapping("/{id}/account")
    public ResponseEntity<?> deleteAccount(@PathVariable Long id, @RequestBody String accountNumber ) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode nameNode = mapper.readTree(accountNumber);
        Customer customer = customerService.getCustomerFullInfo(id);
        if(customer == null) {
            ResponseEntity.badRequest().body("Customer not found");
        }
        return ResponseEntity.ok(customerDtoMapperResponse.convertToDto(customerService.deleteAccount(customer, nameNode.get("accountNumber").asText())));
    }
}


