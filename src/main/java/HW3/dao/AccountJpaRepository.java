package HW3.dao;


import HW3.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface AccountJpaRepository extends JpaRepository<Account, Long> {
    @Query("select e from Account e where e.number = :number")
    Account findByNumber(String number);
}
