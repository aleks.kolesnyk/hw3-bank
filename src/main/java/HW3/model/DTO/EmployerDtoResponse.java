package HW3.model.DTO;

import HW3.model.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EmployerDtoResponse {

    private Long id;
    private String name;
    private String address;
    private Set<Customer> customers;
}

