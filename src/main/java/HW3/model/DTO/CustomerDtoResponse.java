package HW3.model.DTO;

import HW3.model.Account;
import HW3.model.Employer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDtoResponse {

    private Long id;
    private String name;
    private String email;
    private Integer age;
    private String phone;
    private List<Account> accounts;
    private Set<Employer> employers;
}


