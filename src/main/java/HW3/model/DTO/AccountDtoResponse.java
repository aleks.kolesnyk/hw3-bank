package HW3.model.DTO;

import HW3.model.Currency;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDtoResponse {

    private Long id;
    private String number;
    private Currency currency;
    private Double balance;
    private Long userId;
}

